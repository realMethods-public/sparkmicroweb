# Specify the provider and access details 
provider "aws" {
  region = "us-east-1"
  access_key = "${var.aws-access-key}"
  secret_key = "${var.aws-secret-key}"
}


# Our default security group to access
# the instances over SSH and HTTP
resource "aws_security_group" "web" {
  description = "security group created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # SSH access from anywhere
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # mysql access from anywhere
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # HTTP access from the VPC
  ingress {
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Our default security group to for the database
resource "aws_security_group" "rds" {
  description = "security group created from terraform"
  vpc_id      = "vpc-c422e2a0"

  # mysql access from anywhere
  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound internet access
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_db_instance" "default" {
  depends_on             = ["aws_security_group.rds"]
  identifier             = "sparkmicroweb-rds"
  allocated_storage      = "10"
  engine                 = "mysql"
  engine_version         = "8.0.15"
  instance_class         = "db.t2.micro"
  name                   = "sparkmicroweb"
  username               = "root"
  password               = "6969Cutlass!!"
  vpc_security_group_ids = ["${aws_security_group.rds.id}"]
}

resource "aws_instance" "web" {
  # The connection block tells our provisioner how to
  # communicate with the resource (instance)
  connection {
    # The default username for our AMI
    user = "ubuntu"
 }

  instance_type = "t2.micro"
  
  tags { Name = "spark microweb instance" } 

  # standard realmethods community image with docker 
  ami = "ami-05033408e5e831fb0"

  # The name of the  SSH keypair you've created and downloaded
  # from the AWS console. 
  #
  # https://console.aws.amazon.com/ec2/v2/home?region=us-west-2#KeyPairs:
  #
  key_name = "my-public-key"
  
  # Our Security group to allow HTTP and SSH access
  vpc_security_group_ids = ["${aws_security_group.web.id}"]

  # We run a remote provisioner on the instance after creating it.
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get -y update",
      "sudo docker login --username tylertravismya --password 69cutlass",
      "sudo docker pull realmethods/sparkmicrowebdemo:latest",
      "sudo docker run -it -e DATABASE_URL=jdbc:mysql://${aws_db_instance.default.endpoint}/sparkmicroweb?createDatabaseIfNotExist=true -p 8000:8000 realmethods/sparkmicrowebdemo:latest",
    ]
  }
}
