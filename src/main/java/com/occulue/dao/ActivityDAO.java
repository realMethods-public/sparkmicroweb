/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity Activity.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class ActivityDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(Activity.class.getName());

    /**
     * default constructor
     */
    public ActivityDAO() {
    }

    /**
     * Retrieves a Activity from the persistent store, using the provided primary key.
     * If no match is found, a null Activity is returned.
     * <p>
     * @param       pk
     * @return      Activity
     * @exception   ProcessingException
     */
    public Activity findActivity(ActivityPrimaryKey pk)
        throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ActivityDAO.findActivity(...) cannot have a null primary key argument");
        }

        Query query = null;
        Activity businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.Activity as activity where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("activity.activityId = " +
                pk.getActivityId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new Activity();
                businessObject.copy((Activity) query.list().iterator().next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ActivityDAO.findActivity failed for primary key " + pk +
                " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ActivityDAO.findActivity - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all Activitys
     * @return                ArrayList<Activity>
     * @exception   ProcessingException
     */
    public ArrayList<Activity> findAllActivity() throws ProcessingException {
        ArrayList<Activity> list = new ArrayList<Activity>();
        ArrayList<Activity> refList = new ArrayList<Activity>();
        Query query = null;
        StringBuilder buf = new StringBuilder("from com.occulue.bo.Activity");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<Activity>) query.list();

                Activity tmp = null;

                for (Activity listEntry : list) {
                    tmp = new Activity();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ActivityDAO.findAllActivity failed - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ActivityDAO.findAllActivity - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info("ActivityDAO:findAllActivitys() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new Activity into the persistent store.
     * @param       businessObject
     * @return      newly persisted Activity
     * @exception   ProcessingException
     */
    public Activity createActivity(Activity businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ActivityDAO.createActivity - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ActivityDAO.createActivity failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ActivityDAO.createActivity - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided Activity to the persistent store.
     *
     * @param       businessObject
     * @return      Activity        stored entity
     * @exception   ProcessingException
     */
    public Activity saveActivity(Activity businessObject)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ActivityDAO.saveActivity - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException("ActivityDAO.saveActivity failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ActivityDAO.saveActivity - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a Activity from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteActivity(ActivityPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            Activity bo = findActivity(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ActivityDAO.deleteActivity - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ActivityDAO.deleteActivity failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ActivityDAO.deleteActivity - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
