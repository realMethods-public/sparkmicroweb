#propertiesHeader()
FROM adoptopenjdk/openjdk8

######################################################
# installing common packages
######################################################
RUN apt-get update --fix-missing && \
apt-get install -y dos2unix && \
apt-get install -y maven

######################################################
##  final session prep
######################################################
ARG PROJECT=sparkmicroweb
ARG PROJECT_DIR=/var/www/${PROJECT}
RUN mkdir -p $PROJECT_DIR
COPY /target/sparkmicrowebdemo-0.0.1-jar-with-dependencies.jar $PROJECT_DIR/
COPY entrypoint.sh $PROJECT_DIR/
RUN cd $PROJECT_DIR
WORKDIR $PROJECT_DIR

RUN dos2unix /var/www/sparkmicroweb/entrypoint.sh
RUN chmod +x /var/www/sparkmicroweb/entrypoint.sh
ENTRYPOINT ["/var/www/sparkmicroweb/entrypoint.sh"]
