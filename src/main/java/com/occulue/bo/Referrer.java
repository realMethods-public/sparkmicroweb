/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.bo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.occulue.bo.*;

import com.occulue.primarykey.*;

import java.util.*;


/**
 * Encapsulates data for business entity Referrer.
 *
 * @author dev@realmethods.com
 */
@JsonIgnoreProperties(ignoreUnknown = true)
// AIB : #getBOClassDecl()
public class Referrer extends Base {
    // attributes

    // AIB : #getAttributeDeclarations( true  )
    protected Long referrerId = null;
    protected String firstName = null;
    protected String lastName = null;
    public String emailAddress = null;
    public Boolean active = new Boolean("true");
    protected Set<Comment> comments = null;

    // ~AIB

    //************************************************************************
    // Constructors
    //************************************************************************

    /**
     * Default Constructor
     */
    public Referrer() {
    }

    //************************************************************************
    // Accessor Methods
    //************************************************************************

    /**
     * Returns the ReferrerPrimaryKey
     * @return ReferrerPrimaryKey
     */
    public ReferrerPrimaryKey getReferrerPrimaryKey() {
        ReferrerPrimaryKey key = new ReferrerPrimaryKey();
        key.setReferrerId(this.referrerId);

        return (key);
    }

    // AIB : #getBOAccessorMethods(true)
    /**
    * Returns the firstName
    * @return String
    */
    public String getFirstName() {
        return this.firstName;
    }

    /**
                  * Assigns the firstName
        * @param firstName        String
        */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
    * Returns the lastName
    * @return String
    */
    public String getLastName() {
        return this.lastName;
    }

    /**
                  * Assigns the lastName
        * @param lastName        String
        */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
    * Returns the emailAddress
    * @return String
    */
    public String getEmailAddress() {
        return this.emailAddress;
    }

    /**
                  * Assigns the emailAddress
        * @param emailAddress        String
        */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
    * Returns the active
    * @return Boolean
    */
    public Boolean getActive() {
        return this.active;
    }

    /**
                  * Assigns the active
        * @param active        Boolean
        */
    public void setActive(Boolean active) {
        this.active = active;
    }

    /**
    * Returns the Comments
    * @return Set<Comment>
    */
    public Set<Comment> getComments() {
        return this.comments;
    }

    /**
                  * Assigns the comments
        * @param comments        Set<Comment>
        */
    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    /**
    * Returns the referrerId
    * @return Long
    */
    public Long getReferrerId() {
        return this.referrerId;
    }

    /**
                  * Assigns the referrerId
        * @param referrerId        Long
        */
    public void setReferrerId(Long referrerId) {
        this.referrerId = referrerId;
    }

    // ~AIB

    /**
     * Performs a shallow copy.
     * @param object         Referrer                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public Referrer copyShallow(Referrer object)
        throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " Referrer:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        // Set member attributes

        // AIB : #getCopyString( false )
        this.referrerId = object.getReferrerId();
        this.firstName = object.getFirstName();
        this.lastName = object.getLastName();
        this.emailAddress = object.getEmailAddress();
        this.active = object.getActive();

        // ~AIB 
        return this;
    }

    /**
     * Performs a deep copy.
     * @param object         Referrer                copy source
     * @exception IllegalArgumentException         Thrown if the passed in obj is null. It is also
     *                                                         thrown if the passed in businessObject is not of the correct type.
     */
    public Referrer copy(Referrer object) throws IllegalArgumentException {
        if (object == null) {
            throw new IllegalArgumentException(
                " Referrer:copy(..) - object cannot be null.");
        }

        // Call base class copy
        super.copy(object);

        copyShallow(object);

        // Set member attributes

        // AIB : #getCopyString( true )
        if (object.getComments() != null) {
            this.comments = new HashSet<Comment>();

            Comment tmp = null;

            for (Comment listEntry : object.getComments()) {
                tmp = new Comment();
                tmp.copyShallow(listEntry);
                this.comments.add(tmp);
            }
        } else {
            this.comments = null;
        }

        // ~AIB 
        return (this);
    }

    /**
     * Returns a string representation of the object.
     * @return String
     */
    public String toString() {
        StringBuilder returnString = new StringBuilder();

        returnString.append(super.toString() + ", ");

        // AIB : #getToString( false )
        returnString.append("referrerId = " + this.referrerId + ", ");
        returnString.append("firstName = " + this.firstName + ", ");
        returnString.append("lastName = " + this.lastName + ", ");
        returnString.append("emailAddress = " + this.emailAddress + ", ");
        returnString.append("active = " + this.active + ", ");

        // ~AIB 
        return returnString.toString();
    }

    public java.util.Collection<String> getAttributesByNameUserIdentifiesBy() {
        Collection<String> names = new java.util.ArrayList<String>();

        return (names);
    }

    public String getIdentity() {
        StringBuilder identity = new StringBuilder("Referrer");

        identity.append("::");
        identity.append(referrerId);

        return (identity.toString());
    }

    public String getObjectType() {
        return ("Referrer");
    }

    //************************************************************************
    // Object Overloads
    //************************************************************************
    public boolean equals(Object object) {
        Object tmpObject = null;

        if (this == object) {
            return true;
        }

        if (object == null) {
            return false;
        }

        if (!(object instanceof Referrer)) {
            return false;
        }

        Referrer bo = (Referrer) object;

        return (getReferrerPrimaryKey().equals(bo.getReferrerPrimaryKey()));
    }

    // ~AIB
}
