/*******************************************************************************
  Turnstone Biologics Confidential

  2018 Turnstone Biologics
  All Rights Reserved.

  This file is subject to the terms and conditions defined in
  file 'license.txt', which is part of this source code package.

  Contributors :
        Turnstone Biologics - General Release
 ******************************************************************************/
package com.occulue.dao;

import com.occulue.bo.*;

import com.occulue.dao.*;

import com.occulue.exception.ProcessingException;

import com.occulue.primarykey.*;

import org.hibernate.*;

import org.hibernate.cfg.*;

import java.sql.*;

import java.util.*;
import java.util.logging.Logger;


/**
 * Implements the Hibernate persistence processing for business entity ReferenceGroupLink.
 *
 * @author dev@realmethods.com
 */

// AIB : #getDAOClassDecl()
public class ReferenceGroupLinkDAO extends BaseDAO// ~AIB
 {
    // AIB : #outputDAOFindAllImplementations()
    // ~AIB

    //*****************************************************
    // Attributes
    //*****************************************************
    private static final Logger LOGGER = Logger.getLogger(ReferenceGroupLink.class.getName());

    /**
     * default constructor
     */
    public ReferenceGroupLinkDAO() {
    }

    /**
     * Retrieves a ReferenceGroupLink from the persistent store, using the provided primary key.
     * If no match is found, a null ReferenceGroupLink is returned.
     * <p>
     * @param       pk
     * @return      ReferenceGroupLink
     * @exception   ProcessingException
     */
    public ReferenceGroupLink findReferenceGroupLink(
        ReferenceGroupLinkPrimaryKey pk) throws ProcessingException {
        if (pk == null) {
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.findReferenceGroupLink(...) cannot have a null primary key argument");
        }

        Query query = null;
        ReferenceGroupLink businessObject = null;

        StringBuilder fromClause = new StringBuilder(
                "from com.occulue.bo.ReferenceGroupLink as referencegrouplink where ");

        Session session = null;
        Transaction tx = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            // AIB : #getHibernateFindFromClause()
            fromClause.append("referencegrouplink.referenceGroupLinkId = " +
                pk.getReferenceGroupLinkId().toString());
            // ~AIB
            query = session.createQuery(fromClause.toString());

            if (query != null) {
                businessObject = new ReferenceGroupLink();
                businessObject.copy((ReferenceGroupLink) query.list().iterator()
                                                              .next());
            }

            commitTransaction(tx);
        } catch (Throwable exc) {
            businessObject = null;
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.findReferenceGroupLink failed for primary key " +
                pk + " - " + exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.findReferenceGroupLink - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
     * returns a Collection of all ReferenceGroupLinks
     * @return                ArrayList<ReferenceGroupLink>
     * @exception   ProcessingException
     */
    public ArrayList<ReferenceGroupLink> findAllReferenceGroupLink()
        throws ProcessingException {
        ArrayList<ReferenceGroupLink> list = new ArrayList<ReferenceGroupLink>();
        ArrayList<ReferenceGroupLink> refList = new ArrayList<ReferenceGroupLink>();
        Query query = null;
        StringBuilder buf = new StringBuilder(
                "from com.occulue.bo.ReferenceGroupLink");

        try {
            Session session = currentSession();

            query = session.createQuery(buf.toString());

            if (query != null) {
                list = (ArrayList<ReferenceGroupLink>) query.list();

                ReferenceGroupLink tmp = null;

                for (ReferenceGroupLink listEntry : list) {
                    tmp = new ReferenceGroupLink();
                    tmp.copyShallow(listEntry);
                    refList.add(tmp);
                }
            }
        } catch (Throwable exc) {
            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.findAllReferenceGroupLink failed - " +
                exc);
        } finally {
            try {
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.findAllReferenceGroupLink - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        if (list.size() <= 0) {
            LOGGER.info(
                "ReferenceGroupLinkDAO:findAllReferenceGroupLinks() - List is empty.");
        }

        return (refList);
    }

    /**
     * Inserts a new ReferenceGroupLink into the persistent store.
     * @param       businessObject
     * @return      newly persisted ReferenceGroupLink
     * @exception   ProcessingException
     */
    public ReferenceGroupLink createReferenceGroupLink(
        ReferenceGroupLink businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.save(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.createReferenceGroupLink - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.createReferenceGroupLink failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.createReferenceGroupLink - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        // return the businessObject
        return (businessObject);
    }

    /**
     * Stores the provided ReferenceGroupLink to the persistent store.
     *
     * @param       businessObject
     * @return      ReferenceGroupLink        stored entity
     * @exception   ProcessingException
     */
    public ReferenceGroupLink saveReferenceGroupLink(
        ReferenceGroupLink businessObject) throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            session = currentSession();
            tx = currentTransaction(session);

            session.update(businessObject);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.saveReferenceGroupLink - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.saveReferenceGroupLink failed - " + exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.saveReferenceGroupLink - Hibernate failed to close the Session - " +
                    exc);
            }
        }

        return (businessObject);
    }

    /**
    * Removes a ReferenceGroupLink from the persistent store.
    *
    * @param        pk                identity of object to remove
    * @exception    ProcessingException
    */
    public void deleteReferenceGroupLink(ReferenceGroupLinkPrimaryKey pk)
        throws ProcessingException {
        Transaction tx = null;
        Session session = null;

        try {
            ReferenceGroupLink bo = findReferenceGroupLink(pk);

            session = currentSession();
            tx = currentTransaction(session);
            session.delete(bo);
            commitTransaction(tx);
        } catch (Throwable exc) {
            try {
                if (tx != null) {
                    rollbackTransaction(tx);
                }
            } catch (Throwable exc1) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.deleteReferenceGroupLink - Hibernate failed to rollback - " +
                    exc1);
            }

            exc.printStackTrace();
            throw new ProcessingException(
                "ReferenceGroupLinkDAO.deleteReferenceGroupLink failed - " +
                exc);
        } finally {
            try {
                session.flush();
                closeSession();
            } catch (Throwable exc) {
                LOGGER.info(
                    "ReferenceGroupLinkDAO.deleteReferenceGroupLink - Hibernate failed to close the Session - " +
                    exc);
            }
        }
    }
}
